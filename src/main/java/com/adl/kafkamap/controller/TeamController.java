package com.adl.kafkamap.controller;

import com.adl.kafkamap.dto.TeamDTO;
import com.adl.kafkamap.service.TeamService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@CrossOrigin
@RestController
@RequestMapping("api/v1/team")
public class TeamController {

    @Autowired
     private TeamService teamService;

    @ResponseStatus(HttpStatus.CREATED)
    @PostMapping
    public ResponseEntity insert(@RequestBody TeamDTO teamDTO){
        teamService.insert(teamDTO);
        return new ResponseEntity(HttpStatus.CREATED);
    }

}
