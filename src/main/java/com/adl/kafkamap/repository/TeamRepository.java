package com.adl.kafkamap.repository;

import com.adl.kafkamap.entity.Team;
import org.springframework.data.jpa.repository.JpaRepository;

public interface TeamRepository extends JpaRepository<Team, Integer> {
}
