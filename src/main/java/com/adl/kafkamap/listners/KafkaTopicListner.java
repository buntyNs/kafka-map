package com.adl.kafkamap.listners;

import com.adl.kafkamap.service.kafkaService;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.security.jaas.KafkaJaasLoginModuleInitializer;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;


@Component
public class KafkaTopicListner {
    private static final Logger logger = LoggerFactory.getLogger(KafkaTopicListner.class);
    @Autowired
    private kafkaService kafkaService;

    @Value("${keyTabFile}")
    private String keyTab;

    @Value("${principal}")
    private String principal;


    public void listenerOne(ConsumerRecord consumerRecord, Consumer<?,?> consumer) throws Exception{
        logger.info("Listen method started");
        Long startTime = System.currentTimeMillis();

        try {
            kafkaService.messageListner(consumerRecord);


        }catch (Exception e){
            logger.info("Exeception from  listner :" + e);
        }

        logger.info("Listner method ended. resttime : " + (System.currentTimeMillis() - startTime));
    }

    @Bean
    public KafkaJaasLoginModuleInitializer jaasConfig() throws IOException {
        KafkaJaasLoginModuleInitializer jaasConfig = new KafkaJaasLoginModuleInitializer();
        jaasConfig.setControlFlag(KafkaJaasLoginModuleInitializer.ControlFlag.REQUIRED);
        Map<String, String> options = new HashMap<>();
        options.put("useKeyTab", "true");
        options.put("storeKey", "true");
        options.put("keyTab", keyTab);
        options.put("principal", principal);

        jaasConfig.setOptions(options);
        return jaasConfig;
    }

}
