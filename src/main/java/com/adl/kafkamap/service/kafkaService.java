package com.adl.kafkamap.service;

import com.adl.kafkamap.dto.TeamDTO;
import com.adl.kafkamap.entity.Team;
import com.adl.kafkamap.repository.TeamRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class kafkaService {

    @Autowired
    private TeamRepository teamRepository;
    private static final Logger logger = LoggerFactory.getLogger(kafkaService.class);

    public void messageListner(ConsumerRecord consumerRecord) throws Exception{

        ObjectMapper objectMapper = new ObjectMapper();
        String chenged = consumerRecord.value().toString().replaceAll("\\bIMEI\\b","imei").replaceAll("\\bIMSI\\b","imsi");
        TeamDTO teamDto = null;
        try{
            teamDto = objectMapper.readValue(chenged,TeamDTO.class);
            Team team = new Team();
            team.setPmsId(teamDto.getProfileId());
            team.setLatitude(teamDto.getLatitude());
            team.setLongitute(teamDto.getLongitude());
            teamRepository.save(team);
            logger.info("add/update",team.toString());

        }catch (Exception e){
            logger.info("update error");
            e.printStackTrace();
        }

    }
}
