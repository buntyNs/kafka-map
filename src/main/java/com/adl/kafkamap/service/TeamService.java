package com.adl.kafkamap.service;

import com.adl.kafkamap.dto.TeamDTO;
import com.adl.kafkamap.entity.Team;
import com.adl.kafkamap.repository.TeamRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class TeamService {
    @Autowired
    private TeamRepository teamRepository;

    public  void insert(TeamDTO teamDTO) {
        Team team=new Team();
        team.setPmsId(teamDTO.getProfileId());
        team.setLongitute(teamDTO.getLongitude());
        team.setLatitude(teamDTO.getLatitude());
        teamRepository.save(team);
    }

}
