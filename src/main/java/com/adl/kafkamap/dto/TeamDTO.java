package com.adl.kafkamap.dto;


import org.apache.kafka.common.protocol.types.Field;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class TeamDTO {

    private String profileId;
    private double longitude;
    private double latitude;
    private String IMEI;
    private String IMSI;
    private String activity;
    private String agentMsisdn;
    private String agentType;
    private String app_version;
    private String channel;
    private String dateTime;
    private String deviceModel;
    private String device_os_version;
    private String manufacturer;
    private String parentProfileId;
    private String parent_imei;
    private String parent_msisdn;
    private String remark;

    public TeamDTO() {
    }

    public TeamDTO(String profileId, double longitude, double latitude, String IMEI, String IMSI, String activity, String agentMsisdn, String agentType, String app_version, String channel, String dateTime, String deviceModel, String device_os_version, String manufacturer, String parentProfileId, String parent_imei, String parent_msisdn, String remark) {
        this.profileId = profileId;
        this.longitude = longitude;
        this.latitude = latitude;
        this.IMEI = IMEI;
        this.IMSI = IMSI;
        this.activity = activity;
        this.agentMsisdn = agentMsisdn;
        this.agentType = agentType;
        this.app_version = app_version;
        this.channel = channel;
        this.dateTime = dateTime;
        this.deviceModel = deviceModel;
        this.device_os_version = device_os_version;
        this.manufacturer = manufacturer;
        this.parentProfileId = parentProfileId;
        this.parent_imei = parent_imei;
        this.parent_msisdn = parent_msisdn;
        this.remark = remark;
    }


    public String getProfileId() {
        return profileId;
    }

    public void setProfileId(String profileId) {
        this.profileId = profileId;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getIMEI() {
        return IMEI;
    }

    public void setIMEI(String IMEI) {
        this.IMEI = IMEI;
    }

    public String getIMSI() {
        return IMSI;
    }

    public void setIMSI(String IMSI) {
        this.IMSI = IMSI;
    }

    public String getActivity() {
        return activity;
    }

    public void setActivity(String activity) {
        this.activity = activity;
    }

    public String getAgentMsisdn() {
        return agentMsisdn;
    }

    public void setAgentMsisdn(String agentMsisdn) {
        this.agentMsisdn = agentMsisdn;
    }

    public String getAgentType() {
        return agentType;
    }

    public void setAgentType(String agentType) {
        this.agentType = agentType;
    }

    public String getApp_version() {
        return app_version;
    }

    public void setApp_version(String app_version) {
        this.app_version = app_version;
    }

    public String getChannel() {
        return channel;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public String getDateTime() {
        return dateTime;
    }

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }

    public String getDeviceModel() {
        return deviceModel;
    }

    public void setDeviceModel(String deviceModel) {
        this.deviceModel = deviceModel;
    }

    public String getDevice_os_version() {
        return device_os_version;
    }

    public void setDevice_os_version(String device_os_version) {
        this.device_os_version = device_os_version;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    public void setManufacturer(String manufacturer) {
        this.manufacturer = manufacturer;
    }

    public String getParentProfileId() {
        return parentProfileId;
    }

    public void setParentProfileId(String parentProfileId) {
        this.parentProfileId = parentProfileId;
    }

    public String getParent_imei() {
        return parent_imei;
    }

    public void setParent_imei(String parent_imei) {
        this.parent_imei = parent_imei;
    }

    public String getParent_msisdn() {
        return parent_msisdn;
    }

    public void setParent_msisdn(String parent_msisdn) {
        this.parent_msisdn = parent_msisdn;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    @Override
    public String toString() {
        return "TeamDTO{" +
                "profileId='" + profileId + '\'' +
                ", longitude=" + longitude +
                ", latitude=" + latitude +
                ", IMEI='" + IMEI + '\'' +
                ", IMSI='" + IMSI + '\'' +
                ", activity='" + activity + '\'' +
                ", agentMsisdn='" + agentMsisdn + '\'' +
                ", agentType='" + agentType + '\'' +
                ", app_version='" + app_version + '\'' +
                ", channel='" + channel + '\'' +
                ", dateTime='" + dateTime + '\'' +
                ", deviceModel='" + deviceModel + '\'' +
                ", device_os_version='" + device_os_version + '\'' +
                ", manufacturer='" + manufacturer + '\'' +
                ", parentProfileId='" + parentProfileId + '\'' +
                ", parent_imei='" + parent_imei + '\'' +
                ", parent_msisdn='" + parent_msisdn + '\'' +
                ", remark='" + remark + '\'' +
                '}';
    }
}
