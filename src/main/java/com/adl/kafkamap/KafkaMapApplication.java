package com.adl.kafkamap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.kafka.annotation.EnableKafka;

@EnableKafka
@SpringBootApplication
public class KafkaMapApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaMapApplication.class, args);
	}

}
