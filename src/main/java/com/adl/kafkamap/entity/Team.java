package com.adl.kafkamap.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;


@Entity
public class Team {
    @Id
    private String pmsId;
    @Column(precision=10, scale=2)
    private double longitute;
    @Column(precision=10, scale=2)
    private double latitude;

    public Team() {
    }

    public Team(String pmsId, double longitute, double latitude) {
        this.pmsId = pmsId;
        this.longitute = longitute;
        this.latitude = latitude;
    }

    public String getPmsId() {
        return pmsId;
    }

    public void setPmsId(String pmsId) {
        this.pmsId = pmsId;
    }

    public double getLongitute() {
        return longitute;
    }

    public void setLongitute(double longitute) {
        this.longitute = longitute;
    }

    public double getLatitude() {
        return latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    @Override
    public String toString() {
        return "Team{" +
                "pmsId='" + pmsId + '\'' +
                ", longitute=" + longitute +
                ", latitude=" + latitude +
                '}';
    }
}
